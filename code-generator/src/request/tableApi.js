import httpRequest from "@/request/httpRequest.js";

const tableApi = {
    queryTables(data){
        return httpRequest.doGet("db/tables",data)
    },
    queryStruct(data){
        return httpRequest.doGet("db/getTableStruct",data)
    }
}

export default tableApi