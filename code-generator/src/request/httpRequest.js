import axios from 'axios'
const prefix = import.meta.env.VITE_API_BASE_URL
const request = {
    doGet(url,data){
        return axios.get(prefix + url,{
            params: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            }
        });
    },
    doPost(url,data){
        return axios.post(prefix + url, data,{
            headers:{
                'Content-Type': 'application/json',
            }
        });
    }
}

export default request