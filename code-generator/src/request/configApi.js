import httpRequest from "@/request/httpRequest.js";

const configApi = {
    getGlobalConfig(){
        return httpRequest.doGet("config/getGlobalConfig")
    },

    getDateType(){
        return httpRequest.doGet("config/getDateType")
    },

    getIdType(){
        return httpRequest.doGet("config/getIdType")
    },

    updateGlobalConfig(data){
        return httpRequest.doPost("config/updateGlobalConfig",data)
    },

    getPackageConfig(){
        return httpRequest.doGet("config/getPackageConfig")
    },

    updatePackageConfig(data){
        return httpRequest.doPost("config/updatePackageConfig",data)
    },

    getStrategyConfig(){
        return httpRequest.doGet("config/getStrategyConfig")
    },

    updateStrategyConfig(data) {
        return httpRequest.doPost("config/updateStrategyConfig",data)
    },

    generateCode(tableNames){
        return httpRequest.doPost("config/generateCode?tables=" + tableNames)
    }
}

export default configApi