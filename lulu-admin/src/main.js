import { createApp } from 'vue'
import { createPinia } from 'pinia'
import '@/assets/app.css'
import App from './App.vue'
import router from './router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import * as Icons  from '@element-plus/icons-vue'


const app = createApp(App)
app.provide('$Icon', Icons);

for (const [key, component] of Object.entries(Icons)) {
    app.component(key, component)
}
app.use(createPinia())
app.use(router)
app.use(ElementPlus)

app.mount('#app')
