import request from "@/request/httpRequest.js";

const MenuApi = {
    getMenuTree(data){
        return request.doPost("/menu/getMenuTree",data)
    },
    updateStatus(data){
        return request.doPost("/menu/updateMenuStatus",data)
    },
    addMenu(data){
        return request.doPost("/menu/addMenu",data);
    },
    removeMenu(data){
        return request.doPost("/menu/removeMenu",data)
    },
    getMenuInfo(data){
        return request.doGet("/menu/getMenuInfo",data);
    },
    updateMenu(data){
        return request.doPost("/menu/updateMenu",data)
    }
}

export default MenuApi