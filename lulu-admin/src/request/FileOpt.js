import request from "@/request/httpRequest.js";


const FileApi = {
    uploadFile(data){
        return request.doPost("/file/uploadFile",data);
    },
    uploadFiles(data){
        return request.doPost("/file/uploadFiles",data);
    },
    downloadFile(data){
        return request.doGet("/file/download",data);
    },
    downloadFiles(data){
        return request.doGet("/file/downloadFiles",data);
    },
    deleteFile(data){
        return request.doPost("/file/deleteFile",data);
    },
    deleteFiles(data){
        return request.doPost("/file/deleteFiles",data);
    },
    getFileInfo(data){
        return request.doGet("/file/getFileInfo",data);
    },
    getFilePage(data){
        return request.doGet("/file/pageList",data);
    },
    getFileListByPage(data){}
}

export default FileApi;