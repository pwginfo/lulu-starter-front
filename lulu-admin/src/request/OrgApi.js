import request from "@/request/httpRequest.js";

const OrgApi = {
    getOrgTree(){
        return request.doGet("/org/getOrgTree")
    },
    addOrg(data){
        return request.doPost("/org/addOrg",data);
    },
    updateOrg(data){
        return request.doPost("/org/updateOrg",data);
    },
    removeOrg(data){
        return request.doPost("/org/removeOrg",data);
    }
}

export default OrgApi;