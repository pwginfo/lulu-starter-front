import axios from 'axios'
const prefix = import.meta.env.VITE_API_BASE_URL
const request = {
    doGet(url,data){
        return axios.get(prefix + url,{
            params: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "lulu-token": localStorage.getItem("lulu-token")
            }
        });
    },
    doPost(url,data){
        return axios.post(prefix + url, data,{
            headers:{
                'Content-Type': 'application/json',
                "lulu-token": localStorage.getItem("lulu-token")
            }
        });
    },
    doPostWithHeader(url,data,headers){
        return axios.post(prefix + url, data,{
            headers:headers
        });
    }
}

export default request