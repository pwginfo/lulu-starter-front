import request from "@/request/httpRequest.js";

const LoginApi = {
    isLogin(){
        return request.doGet('/auth/isLogin')
    },
    kaptcha(){
        return request.doGet('/auth/getCaptcha?' + Date.now())
    },
    doLogin(data,header){
        return request.doPostWithHeader('/auth/login',data,header)
    },
    logout(data){
        return request.doPost('/auth/logout',data)
    }
}

export default LoginApi;