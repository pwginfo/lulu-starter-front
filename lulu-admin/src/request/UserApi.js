import request from "@/request/httpRequest.js";

const UserApi = {
    addUser(data){
        return request.doPost("/user/addUser",data);
    },
    userInfo(data){
        return request.doGet("/user/userInfo",data);
    },
    updateUser(data){
        return request.doPost("/user/updateUser",data);
    },
    removeUser(data){
        return request.doPost("/user/removeUser",data);
    },
    queryPage(data){
        return request.doPost("/user/queryPage",data);
    },
    getUserInfoByToken(){
        return request.doGet("/user/getUserInfoByToken");
    }
}

export default UserApi;