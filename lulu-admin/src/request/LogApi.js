import request from "@/request/httpRequest.js";

const LogApi = {
    pageList(data){
        return request.doPost("/log/pageList",data);
    }
}

export default LogApi;