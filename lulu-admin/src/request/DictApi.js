import request from "@/request/httpRequest.js";

const DictApi = {
    fetchDictionaries(data){
        return request.doGet('/dictValue/getDictValueByType',data)
    },
    dictTypePage(data){
        return request.doPost('/dictType/pageList',data);
    },
    updateDict(data){
        return request.doPost('/dictType/updateDict',data);
    },
    addDict(data){
        return request.doPost('/dictType/addDict',data);
    },
    removeDict(data){
        return request.doPost('/dictType/removeDict',data);
    },
    dictTypeInfo(data){
        return request.doGet('/dictType/dictTypeInfo',data);
    },


    dictValuePage(data){
        return request.doPost("/dictValue/pageList",data);
    },
    updateDictValue(data){
        return request.doPost("/dictValue/updateDictValue",data);
    },
    dictValueInfo(data){
        return request.doGet("/dictValue/dictValueInfo",data);
    },
    removeDictValue(data){
        return request.doPost("/dictValue/removeDictValue",data);
    },
    addDictValue(data){
        return request.doPost("/dictValue/addDictValue",data);
    }
}

export default DictApi

