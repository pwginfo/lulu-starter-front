export const menuType = [
    {
        key:0,
        value:'目录'
    },
    {
        key:1,
        value:'菜单'
    },
    {
        key:2,
        value:'按钮'
    }
]
export const menuStatus = [
    {
        key:0,
        value:'禁用'
    },
    {
        key:1,
        value:'启用'
    }
]