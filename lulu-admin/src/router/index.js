import { createRouter, createWebHistory } from 'vue-router'
import HomeView from "@/views/HomeView.vue";
import LoginApi from "@/request/LoginApi.js";
import {ElMessage} from "element-plus";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/Login.vue'),
    },
    {
      path: '/main',
      name: 'Main',
      component: () => import('@/views/Main.vue'),
      children:[
        {
          path: '/',
          name: 'home',
          component: HomeView,
          meta: { requiresAuth: true }  // 需要认证
        },
        {
          path: '/sys/menu',
          name: 'menu',
          component: () => import('../components/Menu.vue'),
          meta: { requiresAuth: true }  // 需要认证
        },
        {
          path: '/sys/user',
          name: 'user',
          component: () => import('../components/User.vue'),
          meta: { requiresAuth: true }  // 需要认证
        },
        {
          path: '/sys/log',
          name: 'log',
          component: () => import('../components/Log.vue'),
          meta: { requiresAuth: true }  // 需要认证
        },
        {
          path: '/sys/dictType',
          name: 'dictType',
          component: () => import('../components/DictType.vue'),
          meta: { requiresAuth: true }  // 需要认证
        },
        {
          path: '/sys/fileOpt',
          name: 'fileOpt',
          component: () => import('../components/FileOpt.vue'),
          meta: { requiresAuth: true }  // 需要认证
        }
      ]
    }
  ]
})


router.beforeEach((to, from, next) => {
  // 检查该路由是否需要登录权限
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // 假设有一个方法 `isLoggedIn` 来检查用户是否已登录
    if (!localStorage.getItem("lulu-token")) {
      next({path: '/login'});
    }
    LoginApi.isLogin().then(res => {
      res = res.data;
      if (res.code === 200 && res.data === true) {
        next();
      } else {
        ElMessage.error('当前未登录，请先登录');
        next({path: '/login'});
      }
    }).catch(res => {
      next({path: '/login'});
    })
  }else{
    next();
  }
})
export default router
