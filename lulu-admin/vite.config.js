import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  base:'/',
  server: {
    host: '0.0.0.0',
    port: 5173, // 端口
    cors: true,
    proxy: {
      '/api': { // 请求接口中要替换的标识
        target: 'http://127.0.0.1:8086', // 代理地址
        changeOrigin: true, // 是否允许跨域
        secure: false,
        rewrite: (path) => path.replace(/^\/api/, ''),
      },
    }
  }
})
